import pygame
from pygame.locals import *
import time
pygame.init()
screen = pygame.display.set_mode((600,600))
pygame.display.set_caption('tic tac toe')

red=(255,0,0)
green=(0,255,0)
blue=(0,0,255)
black=(0,0,0)
teal=(0,255,255)
magenta=(255,0,255)
yellow=(255,255,0)
sea_green=(0,255,95)

n={1:'',2:'',3:'',4:'',5:'',6 :'',7:'',8:'',9:''}

chance='x'
pygame.draw.line(screen,sea_green,(200,0),(200,600),10)
pygame.draw.line(screen,sea_green,(400,0),(400,600),10)
pygame.draw.line(screen,sea_green,(0,200),(600,200),10)
pygame.draw.line(screen,sea_green,(0,400),(600,400),10)
pygame.draw.rect(screen,yellow,(0,0,599,599),15)

def x1():
    pygame.draw.line(screen,red,(200,0),(0,200),15)
    pygame.draw.line(screen,red,(0,0),(200,200),15)
    pygame.display.update()
def x2():
    pygame.draw.line(screen,red,(200,0),(400,200),15)
    pygame.draw.line(screen,red,(400,0),(200,200),15)
    pygame.display.update()
def x3():
    pygame.draw.line(screen,red,(400,0),(600,200),15)
    pygame.draw.line(screen,red,(600,0),(400,200),15)
    pygame.display.update()
def x4():
    pygame.draw.line(screen,red,(0,200),(200,400),15)
    pygame.draw.line(screen,red,(200,200),(0,400),15)
    pygame.display.update()
def x5():
    pygame.draw.line(screen,red,(200,200),(400,400),15)
    pygame.draw.line(screen,red,(400,200),(200,400),15)
    pygame.display.update()
def x6():
    pygame.draw.line(screen,red,(600,200),(400,400),15)
    pygame.draw.line(screen,red,(400,200),(600,400),15)
    pygame.display.update()
def x7():
    pygame.draw.line(screen,red,(0,400),(200,600),15)
    pygame.draw.line(screen,red,(200,400),(0,600),15)
    pygame.display.update()
def x8():
    pygame.draw.line(screen,red,(400,400),(200,600),15)
    pygame.draw.line(screen,red,(200,400),(400,600),15)
    pygame.display.update()
def x9():
    pygame.draw.line(screen,red,(400,400),(600,600),15)
    pygame.draw.line(screen,red,(600,400),(400,600),15)
    pygame.display.update()

def o1():
    pygame.draw.circle(screen,sea_green,(100,100),100,15)
    pygame.display.update()
def o2():
    pygame.draw.circle(screen,sea_green,(300,100),100,15)
    pygame.display.update()
def o3():
    pygame.draw.circle(screen,sea_green,(500,100),100,15)
    pygame.display.update()
def o4():
    pygame.draw.circle(screen,sea_green,(100,300),100,15)
    pygame.display.update()
def o5():
    pygame.draw.circle(screen,sea_green,(300,300),100,15)
    pygame.display.update()
def o6():
    pygame.draw.circle(screen,sea_green,(500,300),100,15)
    pygame.display.update()
def o7():
    pygame.draw.circle(screen,sea_green,(100,500),100,15)
    pygame.display.update()
def o8():
    pygame.draw.circle(screen,sea_green,(300,500),100,15)
    pygame.display.update()
def o9():
    pygame.draw.circle(screen,sea_green,(500,500),100,15)
    pygame.display.update()
    

def win(var):
    if n[1]==n[2]==n[3]==var or n[4]==n[5]==n[6]==var \
    or n[7]==n[8]==n[9]==var or n[1]==n[4]==n[7]==var \
    or n[2]==n[5]==n[8]==var or n[3]==n[6]==n[9]==var \
    or n[1]==n[5]==n[9]==var or n[3]==n[5]==n[7]==var:
        return True
    return False
def text():
    if win('x'):
        show_text('X Wins!',10,150,blue)
        time.sleep(3)
        quit()
    if win('o'):
        show_text('O Wins!',10,150,blue)
        time.sleep(3)
        quit()
        
def tie():
    if n[1] != '' and n[2] != '' and n[3] != '' \
       and n[4] != '' and n[5] != '' and n[6] != '' \
       and n[7] != '' and n[8] != '' and n[9] != '':
        show_text('Tie!',150,150,blue)
        time.sleep(3)
        quit()

def show_text(msg,x,y,color): 
    fontobj=pygame.font.SysFont("freesans",160)
    msgobj=fontobj.render(msg,False,color)
    screen.blit(msgobj,(x,y))
    pygame.display.update()

while True:
    #screen.blit(img,(250,260))
    print(n)
    pygame.display.update()
    for event in pygame.event.get():
        if event.type==QUIT:
            pygame.quit()
            exit()
        elif event.type==pygame.MOUSEBUTTONDOWN and event.button==1:
            print('you pressed the left mouse button',event.pos)
            x,y = event.pos
            if x in range(0,200) and y in range(0,200):
                if chance=='x' and n[1]=='': 
                    x1()
                    n[1]="x"
                    chance='o'
                    text()
                    tie()
                elif n[1]=='':
                    o1()
                    n[1]='o'
                    chance='x'
                    text()
                    tie()
            if x in range(200,400) and y in range(0,200):
                 if chance=='x' and n[2]=='':
                    x2()
                    n[2]="x"
                    chance='o'
                    text()
                    tie()
                 elif n[2]=='':
                    o2()
                    n[2]='o'
                    chance='x'
                    text()
                    tie()
            if x in range(400,600) and y in range(0,200):
                 if chance=='x' and n[3]=='':
                    x3()
                    n[3]="x"
                    chance='o'
                    text()
                    tie()
                 elif n[3]=='':
                    o3()
                    n[3]='o'
                    chance='x'
                    text()
                    tie()
            if x in range(0,200) and y in range(200,400):
                  if chance=='x' and n[4]=='':
                    x4()
                    n[4]="x"
                    chance='o'
                    text()
                    tie()
                  elif n[4]=='':
                    o4()
                    n[4]='o'
                    chance='x'
                    text()
                    tie()
            if x in range(200,400) and y in range(200,400):
                if chance=='x' and n[5]=='':
                    x5()
                    n[5]="x"
                    chance='o'
                    text()
                    tie()
                elif n[5]=='': 
                    o5()
                    n[5]='o'
                    chance='x'
                    text()
                    tie()
            if x in range(400,600) and y in range(200,400):
                 if chance=='x' and n[6]=='':
                    x6()
                    n[6]="x"
                    chance='o'
                    text()
                    tie()
                 elif n[6]=='':
                    o6()
                    n[6]='o'
                    chance='x'
                    text()
                    tie()
            if x in range(0,200) and y in range(400,600):
                 if chance=='x' and n[7]=='':
                    x7()
                    n[7]="x"
                    chance='o'
                    text()
                    tie()
                 elif n[7]=='': 
                    o7()
                    n[7]='o'
                    chance='x'
                    text()
                    tie()
            if x in range(200,400) and y in range(400,600):
                 if chance=='x' and n[8]=='':
                    x8()
                    n[8]="x"
                    chance='o'
                    text()
                    tie()
                 elif n[8]=='':
                    o8()
                    n[8]='o'
                    chance='x'
                    text()
                    tie()
            if x in range(400,600) and y in range(400,600):
                 if chance=='x' and n[9]=='':
                    x9()
                    n[9]="x"
                    chance='o'
                    text()
                    tie()
                 elif n[9]=='':
                    o9()
                    n[9]='o'
                    chance='x'
                    text()
                    tie()
                
        

        
        
            

