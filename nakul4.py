import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)

GPIO.setup(4,GPIO.OUT)

GPIO.setup(17,GPIO.OUT)

GPIO.setup(18,GPIO.OUT)

GPIO.output(4,GPIO.HIGH)
GPIO.output(4,GPIO.LOW)

p = GPIO.PWM(4,50)

p.start(50)

for n in range(1,100,10):
    p.ChangeFrequency(n)
    time.sleep(10)
    print(n)
    p.ChangeDutyCycle(n)

for n in range(100,1,-10):
    p.ChangeFrequency(n)
    time.sleep(10)
    print(n)
    p.ChangeDutyCycle(n)

p.stop()  
GPIO.output(17,GPIO.HIGH)
GPIO.output(17,GPIO.LOW)

m = GPIO.PWM(17,50)

m.start(50)

for n in range(1,100,10):
    
    m.ChangeFrequency(n)
    time.sleep(1)
    print(n)
    m.ChangeDutyCycle(n)

for n in range(100,1,-10):
    m.ChangeFrequency(n)
    time.sleep(1)
    print(n)
    m.ChangeDutyCycle(n)
m.stop()


GPIO.output(18,GPIO.HIGH)
GPIO.output(18,GPIO.LOW)

o = GPIO.PWM(22,50)

o.start(50)

for n in range(1,100,10):
    
    o.ChangeFrequency(n)
    time.sleep(1)
    print(n)
    o.ChangeDutyCycle(n)

for n in range(100,1,-10):
    o.ChangeFrequency(n)
    time.sleep(1)
    print(n)
    o.ChangeDutyCycle(n)
o.stop()
GPIO.cleanup()















































