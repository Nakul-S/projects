import RPi.GPIO as GPIO
import time
from threading import Thread
GPIO.setmode(GPIO.BCM)

GPIO.setup(15,GPIO.OUT)

GPIO.setup(14,GPIO.OUT)

GPIO.setup(3,GPIO.OUT)

GPIO.setup(2,GPIO.OUT)

GPIO.setup(21,GPIO.IN)





n = GPIO.PWM (15,50)
m = GPIO.PWM (14,50)
o = GPIO.PWM (3,50)
p = GPIO.PWM (2,50)

n.start(50)
m.start(50)
p.start(50)
o.start(50)
speed=50
def accelerate():
    global speed
    print(speed) 
    n.ChangeDutyCycle(speed)
    o.ChangeDutyCycle(speed)
    if speed  < 95:
        speed=speed+5

def decelerate():
    global speed
    print(speed)
    m.ChangeDutyCycle(speed)
    p.ChangeDutyCycle(speed)
    if speed > 5:
        speed=speed-5


        

    
def backward():                             
    m.start(speed)
    
    o.stop()
    
    n.stop()
    
    p.start(speed)
    
def forward():
    n.start(speed)
    
    o.start(speed)
    
    m.stop()
    
    p.stop()

def left():
    m.stop()
    
    n.stop()
    
    p.stop()
    
    o.start(speed)
    
def right():
    o.start(speed)
    
    p.start(speed)
    
    m.stop()
    
    n.start(speed)

def stop():
    p.stop()
    
    o.stop()
    
    m.stop()
    
    n.stop()

def _exit(): 
    p.stop()
    
    o.stop()
    
    m.stop()
    
    n.stop()
    
    GPIO.cleanup()

    exit()

def sensor():
    while True:
        a=GPIO.input(21)
        if a==0:
            stop()

def getch():
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch

def controls():
    while True:
        print("w=forward, s=backward, a=left, d=right, f=stop, e=exit, l=decelerate, o=accelerate")
        ch = getch()
        if ch == 'w':
            forward()
        if ch == 's':
            backward()
        if ch == 'a':
            left()
        if ch == 'd':
            right()    
        if ch == 'f':
            stop()
        if ch == 'e':
            _exit()
        if ch == 'o':
            accelerate()
        if ch == 'l':
            decelerate()


t1=Thread(target=sensor)
t2=Thread(target=controls)

t1.start()
t2.start()
























































      
