#import
import pygame
import random
import time
from pygame.locals import*
pygame.init()
screen = pygame.display.set_mode((600,600))
pygame.display.set_caption("TicTacToe")
#colors
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
magenta = (195,0,196)
teal = (9,218,178)
lavender = (150,83,228)
pink = (255,0,163)
yellow = (237,223,4)
skyblue = (0,206,255)

white = (255,255,255)
black = (0,0,0)
#checks for a tie
global tie
tie = 0
draw = 0
k={1:'_',2:'_',3:'_',4:'_',5:'_',6 :'_',7:'_',8:'_',9:'_'}
player=1
n={1:[0,0],2:[0,200],3:[0,400],4:[200,0],5:[200,200],6:[200,400],
   7:[400,0],8:[400,200],9:[400,400]}



#functions for drawing x
def drawx1():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 0 and v[1] ==0 and (k[1]=='x' or k[1] == 'o'):
            return
    k[1] = 'x'    
    pygame.draw.line(screen, magenta,(0,0),(200,200),3)
    pygame.draw.line(screen, magenta,(0,200),(200,0),3)
    
def drawx2():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 0 and v[1] ==200 and (k[2]=='x' or k[2] == 'o'):
            return
    k[2] = 'x'    
    pygame.draw.line(screen, magenta,(200,0),(400,200),3)
    pygame.draw.line(screen, magenta,(200,200),(400,0),3)
    
def drawx3():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 0 and v[1] ==400 and (k[3]=='x' or k[3] == 'o'):
            return
    k[3] = 'x'    
    pygame.draw.line(screen, magenta,(400,0),(600,200),3)
    pygame.draw.line(screen, magenta,(400,200),(600,0),3)
    
def drawx4():
    print("k", k[1])

    for key,v in n.items():
        if v[0] == 200 and v[1] ==0 and (k[4]=='x' or k[4] == 'o'):
            return
    k[4] = 'x'
    pygame.draw.line(screen, magenta,(0,200),(200,400),3)
    pygame.draw.line(screen, magenta,(0,400),(200,200),3)
    
def drawx5():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 200 and v[1] ==200 and (k[5]=='x' or k[5] == 'o'):
            return
    k[5] = 'x'    
    pygame.draw.line(screen, magenta,(200,200),(400,400),3)
    pygame.draw.line(screen, magenta,(200,400),(400,200),3)
    
def drawx6():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 200 and v[1] ==400 and (k[6]=='x' or k[6] == 'o'):
            return
    k[6] = 'x'    
    pygame.draw.line(screen, magenta,(600,200),(400,400),3)
    pygame.draw.line(screen, magenta,(600,400),(400,200),3)
    
def drawx7():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 400 and v[1] ==0 and (k[7]=='x' or k[7] == 'o'):
            return
    k[7] = 'x'    
    pygame.draw.line(screen, magenta,(0,400),(200,600),3)
    pygame.draw.line(screen, magenta,(0,600),(200,400 ),3)
    
def drawx8():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 400 and v[1] ==200 and (k[7]=='x' or k[7] == 'o'):
            return
    k[8] = 'x'    
    pygame.draw.line(screen, magenta,(200,400),(400,600),3)
    pygame.draw.line(screen, magenta,(200,600),(400,400),3)
    
def drawx9():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 400 and v[1] ==400 and (k[8]=='x' or k[8] == 'o'):
            return
    k[9] = 'x'    
    pygame.draw.line(screen, magenta,(400,400),(600,600),3)
    pygame.draw.line(screen, magenta,(400,600),(600,400),3)

#functions for drawing o
def drawo1():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 0 and v[1] ==0 and (k[9]=='x' or k[9] == 'o'):
            return
    k[1] = 'o'    
    pygame.draw.circle(screen,teal,(100,100),97,3)
def drawo2():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 0 and v[1] ==200 and (k[2]=='x' or k[2] == 'o'):
            return
    k[2] = 'o'     
    pygame.draw.circle(screen,teal,(300,100),97,3)
def drawo3():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 0 and v[1] ==400 and (k[3]=='x' or k[3] == 'o'):
            return
    k[3] = 'o'    
    pygame.draw.circle(screen,teal,(500,100),97,3)
def drawo4():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 200 and v[1] ==0 and (k[4]=='x' or k[4] == 'o'):
            return
    k[4] = 'o'     
    pygame.draw.circle(screen,teal,(100,300),97,3)
def drawo5():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 200 and v[1] ==200 and (k[5]=='x' or k[5] == 'o'):
            return
    k[5] = 'o'    

    pygame.draw.circle(screen,teal,(300,300),97,3)
def drawo6():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 200 and v[1] ==400 and (k[6]=='x' or k[6] == 'o'):
            return
    k[6] = 'o'    
    pygame.draw.circle(screen,teal,(500,300),97,3)
def drawo7():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 400 and v[1] ==0 and (k[7]=='x' or k[7] == 'o'):
            return
    k[7] = 'o'    

    pygame.draw.circle(screen,teal,(100,500),97,3)
def drawo8():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 400 and v[1] ==200 and (k[8]=='x' or k[8] == 'o'):
            return
    k[8] = 'o'    
    pygame.draw.circle(screen,teal,(300,500),97,3)
def drawo9():
    print("k", k[1])
    for key,v in n.items():
        if v[0] == 400 and v[1] ==400 and (k[9]=='x' or k[9] == 'o'):
            return
    k[9] = 'o'    
    pygame.draw.circle(screen,teal,(500,500),97,3)
    
#functions to tell who won and who lost
def win():
    if k[1]==k[2]==k[3]=='x' or k[4]==k[5]==k[6]=='x'\
    or k[7]==k[8]==k[9]=='x' or k[1]==k[4]==k[7]=='x' \
    or k[2]==k[5]==k[8]=='x' or k[3]==k[6]==k[9]=='x' \
    or k[1]==k[5]==k[9]=='x' or k[3]==k[5]==k[7]=='x':
        tie=1
        print('X wins!') 
        show_text ("X wins",150,300,blue)
       
        pygame.display.update()
        time.sleep(5)
        quit()
    if k[1]==k[2]==k[3]=='o' or k[4]==k[5]==k[6]=='o'\
    or k[7]==k[8]==k[9]=='o' or k[1]==k[4]==k[7]=='o' \
    or k[2]==k[5]==k[8]=='o' or k[3]==k[6]==k[9]=='o' \
    or k[1]==k[5]==k[9]=='o' or k[3]==k[5]==k[7]=='o':
        print('O wins!')
        tie=1
#function for typing on the screen
        show_text ("O wins",150,300,blue)
        pygame.display.update()
        time.sleep(5)
        quit
def show_text(msg,x,y,white):
    fontobj=pygame.font.SysFont("freesans",100)
    msgobj=fontobj.render(msg,False,blue)
    screen.blit(msgobj,(x,y))


#player

    


#for making the tic tac toe board
while True:
    pygame.draw.rect(screen, skyblue, (200,0,200,600), 5)
    pygame.draw.rect(screen, skyblue, (0,200,600,200), 5)
    pygame.draw.rect(screen,skyblue, (0,0,599,599), 5)
    pygame.display. update()
    win()
    
    draw = 0
    for q in k:
        if k[q]  ==  '_':
            draw += 1
    #print ("Draw", draw, tie)
    if draw == 0 and tie == 0:
        show_text("Draw!",175,300,blue)
    for event in pygame.event.get():
        if event.type ==QUIT:
            pygame.quit()
            exit()
#event,pos
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            #print ("You pressed the left mouse button at:", event.pos)
            x=event.pos[0]
            
            y=event.pos[1]
#for drawing x's AND o's
            print(k)
            if x in range (0,200) and y in range(0,200):
                
                if player==1 and k[1]=="_":
                    drawx1()
                    player=2
                elif player==2 and k[1]=="_":   
                    drawo1()
                    player=1
                    
            if x in range (200,400) and y in range(0,200):
                print(player, k[2])
                if player==1 and k[2]=="_":   
                    drawx2()
                    player=2
                elif player==2 and k[2]=="_":
                    drawo2()
                    player=1
                    
            if x in range (400,600) and y in range(0,200):
                if player==1 and k[3]=="_":
                    drawx3()
                    player=2
                elif player==2 and k[3]=="_":
                    drawo3()
                    player=1
                    
            if x in range (0,200) and y in range(200,400):
                if player==1 and k[4]=="_":
                    drawx4()
                    player=2
                elif player==2 and k[4]=="_":
                    drawo4()
                    player=1
                    
            if x in range (200,400) and y in range(200,400):
                if player==1 and k[5]=="_":
                    drawx5()
                    player=2
                elif player==2 and k[5]=="_":
                    drawo5()
                    player=1
                    
            if x in range (400,600) and y in range(200,400):
                if player==1 and k[6]=="_":
                    drawx6()
                    player=2
                elif player==2 and k[6]=="_":
                    drawo6()
                    player=1
                    
            if x in range (0,200) and y in range(400,600):
                if player==1 and k[7]=="_":
                    drawx7()
                    player=2
                elif player==2 and k[7]=="_":
                    drawo7()
                    player=1
                    
            if x in range (200,400) and y in range(400,600):
                if player==1 and k[8]=="_":
                    drawx8()
                    player=2
                elif player==2 and k[8]=="_":
                    drawo8()
                    player=1
                    
            if x in range (400,600) and y in range(400,600):
                if player==1 and k[9]=="_":
                    drawx9()
                    player=2
                elif player==2 and k[9]=="_":
                    drawo9()
                    player=1
                    
                
        
        

