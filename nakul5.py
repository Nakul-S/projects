import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)

GPIO.setup(6,GPIO.OUT)

GPIO.setup(19,GPIO.OUT)

GPIO.setup(16,GPIO.OUT)

p = GPIO.PWM(6,50)
m = GPIO.PWM(19,50)
o = GPIO.PWM(16,50)

p.start(20)

m.start(20)

o.start(20)
while True:
    p.ChangeFrequency(20)
    
    m.ChangeFrequency(40)
    
    m.ChangeFrequency(60)

    time.sleep(10)    
GPIO.cleanup()
